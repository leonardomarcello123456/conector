# TEMPLATE CHANGELOG (ALTERAR)

# {version}
[{date}]

### Quebras de compatibilidade
* N/A.

### Novas funcionalidades
* [TECBPM-510](https://jira.senior.com.br/browse/TECBPM-510) - Criando conector para iniciar uma nova solicitação.
* [TECBPM-588](https://jira.senior.com.br/browse/TECBPM-588) - Criação conector para cancelar instância do processo.

### Melhorias
* [TECBPM-510](https://jira.senior.com.br/browse/TECBPM-510) - Criando conector para iniciar uma nova solicitação.

### Correções
* N/A.

### Alterações na base de dados
* N/A.

### Alteração de dependências
* N/A.
