import axios from 'axios';
import { middyfy } from '../../libs/lambda';

const getBpmAllDocumentsTickets = '/platform/workflow/queries/getAllDocumentTickets';
const getDocsId = '/platform/ecm_ged/queries/newDocumentStatus';
const getDocsVersion = '/platform/ecm_ged/queries/getDocumentVersions';
const signDocs = '/platform/ecm_ged/actions/signDocuments';

const bpmDocsEnvelope = async (event) => {
  try {

    const { processInstanceId, instructionsToSigner, signerName, signerEmail, signerPhoneNumber } = event.body;

    const getBpmAllDocumentsTicketsUrl = event.headers['x-platform-environment'] + getBpmAllDocumentsTickets;
    const getDocsIdUrl = event.headers['x-platform-environment'] + getDocsId;
    const getDocsVersionUrl = event.headers['x-platform-environment'] + getDocsVersion;
    const signDocsUrl = event.headers['x-platform-environment'] + signDocs;

    let documentsTicket: string[] = []; 
    await axios.get(`${getBpmAllDocumentsTicketsUrl}?processInstanceId=${processInstanceId}`, {
      headers: { Authorization: event.headers["x-platform-authorization"] }
    }).then(resp => {
      documentsTicket = resp.data.tickets;
    });

    const documentsId: string[] = []
    for (const ticket of documentsTicket) {
      await axios.get(`${getDocsIdUrl}?ticket=${ticket}`, {
        headers: { Authorization: event.headers["x-platform-authorization"] }
      }).then(resp => {
        documentsId.push(resp.data.documentId);
      });
    }

    const documentVersionIds: string[] = [];
    for (const documentId of documentsId) {
      await axios.get(`${getDocsVersionUrl}?documentId=${documentId}`, {
        headers: { Authorization: event.headers["x-platform-authorization"] }
      }).then(resp => {
        documentVersionIds.push(resp.data.documentVersions[0].id)
      });
    }

    const signBody = {
      documentVersionIds,
      signers: [{
        name: signerName,
        email: signerEmail,
        phoneNumber: signerPhoneNumber
      }],
      instructionsToSigner,
      askGeolocation: "DONT_ASK_LOCATION",
      notifyUser: false,
      senEmail: false 
    }

    const responseData = await axios.post(signDocsUrl, signBody, {
      headers: { Authorization: event.headers["x-platform-authorization"] }
    });    

    return {
      statusCode: responseData?.status || 200,
      body: JSON.stringify(responseData?.data)
    };
  } catch (error) {
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    }
  }
};

export const main = middyfy(bpmDocsEnvelope);
