import { handlerPath } from '../../libs/handler-resolver';

export default {
  handler: `${handlerPath(__dirname)}/handler.main`,
  events: [
    {
      http: {
        // Os métodos podem ser post, get, put e delete
        method: 'get',
        // Não esqueça de alterar aqui para o nome da pasta do seu conector
        path: 'teste'
      },
    },
  ],
};
