import { middyfy } from '../../libs/lambda';
import convert from 'xml-js';

import { createClientAsync } from 'soap';

const url = 'https://chemspider.com/MassSpecAPI.asmx?WSDL';

const handler = async (event) => {

  try {
    const cliente = await createClientAsync(url);
    const response = await cliente.GetDatabasesAsync({});

    const json = convert.xml2json(response[1], { compact: true, spaces: 4 });

    return {
      statusCode: 200,
      body: JSON.stringify(json)
    };
  } catch (err) {
    return {
      statusCode: 500,
      body: JSON.stringify(err)
    };
  }

};

export const main = middyfy(handler);