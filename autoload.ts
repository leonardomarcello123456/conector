// eslint-disable-next-line @typescript-eslint/no-var-requires
const fs = require('fs');

export const autoload = () => {

  const files = fs.readdirSync(`${__dirname}/conectores`);

  const folders = files //
    .filter(file => fs.lstatSync(`${__dirname}/conectores/${file}`).isDirectory()) //
    .filter(path => path !== 'libs' && path !== 'node_modules' && path !== '.esbuild');

  const conectors = {};
  for (const folder of folders) {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    conectors[folder] = require(`${__dirname}/conectores/${folder}/index.ts`).default;
  }

  const exemples = fs.readdirSync(`${__dirname}/exemplos`);

  const foldersSamples = exemples //
    .filter(file => fs.lstatSync(`${__dirname}/exemplos/${file}`).isDirectory()) //
    .filter(path => path !== 'libs' && path !== 'node_modules' && path !== '.esbuild');

  for (const folder of foldersSamples) {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    conectors[folder] = require(`${__dirname}/exemplos/${folder}/index.ts`).default;
  }

  return conectors;
}